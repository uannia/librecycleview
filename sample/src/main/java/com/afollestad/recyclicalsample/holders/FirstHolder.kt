/**
 * Designed and developed by Aidan Follestad (@afollestad)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.afollestad.recyclicalsample.holders

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.afollestad.recyclical.*
import com.afollestad.recyclicalsample.R
import com.afollestad.recyclicalsample.model.MyListItem

class FirstHolder(itemView: View) : ViewHolder(itemView) {
    val title: TextView = itemView.findViewById(R.id.txt)
    val recycler: RecyclerView = itemView.findViewById(R.id.recycle_first)
    private val dataSource = emptySelectableDataSource()
    private var vitri = -1


    fun bindData(list: Int) {
        dataSource.set(
            IntArray(list) { it + 1 }
                .map {
                    MyListItem(
                        title = "#$it",
                        body = "Hello, world! #$it"
                    )
                }
        )

        recycler.setup {
            withDataSource(dataSource)
            //withItem<MyListItem>(R.layout.my_list_item) {

        }
    }
}
